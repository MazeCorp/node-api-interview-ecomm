var User = require('../models/User');
var Product = require('../models/Product');
var mongoose = require('mongoose');
const mongoConfig = require('../configs/mongo-config')
mongoose.connect(mongoConfig, { useNewUrlParser: true, useCreateIndex: true, });


var products =
	[
		new Product({
			productId: 1,
			name: 'Old Camera',
			description: 'Bad quality camera.',
			price: 5590.50,
			make: 1995,
		}),
		new Product({
			productId: 2,
			name: 'Kodak Camera',
			description: 'Good quality camera.',
			price: 45590.50,
			make: 1995,
		}),
		new Product({
			productId: 3,
			name: 'Nikon Coolpix',
			description: 'Good quality camera.',
			price: 45590.50,
			make: 1995,
		}),
		new Product({
			productId: 4,
			name: 'Sony Pixel S787',
			description: 'Good quality camera.',
			price: 45590.50,
			make: 1995,
		}),
		new Product({
			productId: 5,
			name: 'Canon C4646',
			description: 'Good quality camera.',
			price: 45590.50,
			make: 1995,
		}),
		new Product({
			productId: 6,
			name: 'Nikon D850',
			description: 'Good quality camera.',
			price: 38449.00,
			make: 1995,
		}),
	];

for (let i = 0; i < products.length; i++) {
	products[i].save(function (e, r) {
		if (i === products.length - 1) {
			exit();
		}
	});
}

var newUser = new User({
	userId: 1,
	username: 'suraj.kumar@admin.com',
	password: 'admin@123',
	fullname: 'Suraj Kumar',
	admin: true
});
User.createUser(newUser, function (err, user) {
	if (err) throw err;
	console.log(user);
});

function exit() {
	mongoose.disconnect();
}