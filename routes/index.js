var express = require('express');
var router = express.Router();
const Product = require('../models/Product')
const TypedError = require('../modules/ErrorHandler')
const Cart = require('../models/Cart');
const CartClass = require('../modules/Cart')


//GET /products
router.get('/products', function (req, res, next) {
	const { query, order } = categorizeQueryString(req.query)
	Product.getAllProducts(query, order, function (e, products) {
		if (e) {
			e.status = 406; return next(e);
		}
		if (products.length < 1) {
			return res.status(404).json({ message: "products not found" })
		}
		res.json({ products: products })
	})
});

//GET /products/:id
router.get('/products/:id', function (req, res, next) {
	let productId = req.params.id;
	Product.getProductByID(productId, function (e, item) {
		if (e) {
			e.status = 404; return next(e);
		}
		else {
			res.json({ product: item })
		}
	});
});

// GET filter
router.get('/filter', function (req, res, next) {
	let result = {}
	let query = req.query.query
	Product.filterProductByDepartment(query, function (err, p) {
		if (err) return next(err)
		if (p.length > 0) {
			result['department'] = generateFilterResultArray(p, 'department')
		}
		Product.filterProductByCategory(query, function (err, p) {
			if (err) return next(err)
			if (p.length > 0) {
				result['category'] = generateFilterResultArray(p, 'category')
			}
			Product.filterProductByTitle(query, function (err, p) {
				if (err) return next(err)
				if (p.length > 0) {
					result['title'] = generateFilterResultArray(p, 'title')
				}
				if (Object.keys(result).length > 0) {
					return res.json({ filter: result })
				} else {
					let error = new TypedError('search', 404, 'not_found', { message: "no product exist" })
					return next(error)
				}
			})
		})
	})
})



function generateFilterResultArray(products, targetProp) {
	let result_set = new Set()
	for (const p of products) {
		result_set.add(p[targetProp])
	}
	return Array.from(result_set)
}

function categorizeQueryString(queryObj) {
	let query = {}
	let order = {}
	//extract query, order, filter value
	for (const i in queryObj) {
		if (queryObj[i]) {
			// extract order
			if (i === 'order') {
				order['sort'] = queryObj[i]
				continue
			}
			// extract range
			if (i === 'range') {
				let range_arr = []
				let query_arr = []
				// multi ranges
				if (queryObj[i].constructor === Array) {
					for (const r of queryObj[i]) {
						range_arr = r.split('-')
						query_arr.push({
							price: { $gt: range_arr[0], $lt: range_arr[1] }
						})
					}
				}
				// one range
				if (queryObj[i].constructor === String) {
					range_arr = queryObj[i].split('-')
					query_arr.push({
						price: { $gt: range_arr[0], $lt: range_arr[1] }
					})
				}
				Object.assign(query, { $or: query_arr })
				delete query[i]
				continue
			}
			query[i] = queryObj[i]
		}
	}
	return { query, order }
}

module.exports = router;
