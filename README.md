**INTERVIEW | NODE | ECOOMERCE | API | DEMO**


## Setup & Run

Follow the below instructions

1. **Install dependencies**: by `npm install`.
2. **Populate data**: run  `npm run migration` (make sure you fill up `mongo-config` file with proper mongodb url).
3. **Start**: `npm start`.

---

## API CALLS
