var mongoose = require('mongoose');

var productSchema = mongoose.Schema({
  name: {
    type: String
  },
  description: {
    type: String
  },
  price: {
    type: Number
  },
  make: {
    type: Number
  }
});

var Product = module.exports = mongoose.model('Product', productSchema);

module.exports.getAllProducts = function (query, sort, callback) {
  Product.find(query, null, sort, callback)
}

module.exports.getProductByName = function (query,sort, callback) {
  Product.find(query, null, sort, callback)
}

module.exports.filterProductByName = function (name, callback) {
  let regexp = new RegExp(`${name}`, 'i')
  var query = { name: { $regex: regexp } };
  Product.find(query, callback);
}

module.exports.getProductByID = function (id, callback) {
  Product.findById(id, callback);
}

